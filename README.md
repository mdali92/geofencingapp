# GeoFencingApp

The purpose of this App is to create a geofence based on the following criterias :

1) Latitude and Longitude
2) Radius
3) Wifi SSID

## How to Use

 - Initially on first time app launch, there are not geofence set. So, it will always show 'Outside Geofence' message although this aspect of UI/UX can be improved laters on.
- Enter the latitude
- Enter the longitude
- Enter the Radius in meters
- Enter the Wifi SSID 
- Press 'Start Monitoring'

## Caveats/Issues
- The official Google's GeoFencing API is not the best way to implement Geofecing as it often outputs incorrect and inconsistent results. The proper and (lengthy) method is to poll the LocationManager to get GPS coordinate (at frequent intervals) manually and perform client side calculation of the radius. This is more consistent and accurate. However, for this assignment's scope, the GeoFencing API was used.
- Because of the above, the GeoFencing API fails to send broadcast when entering/exiting the  Geofence Zone in some rare occasion. Dirty hack is to toggle off and then on the Location service of the phone which resets the Geofencing API and kicks it back to work. Proper solution ofcourse is to NOT rely on Geofencing API and do manual implementation of the Geofencing calculation.
