package com.azhar.geofencingapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingEvent

class GeofenceBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val geofencingEvent = GeofencingEvent.fromIntent(intent)
        if (geofencingEvent.hasError()) {
            //do nothign
        }
        // Get the transition type.
        val geofenceTransition = geofencingEvent.geofenceTransition
        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            Log.d("Azz","ENTERED GEOFENCE!")
            RxBus.publish(RxEvent.EventGeoFence("enter"))
        } else if(geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            Log.d("Azz","EXITED GEOFENCE!")
            RxBus.publish(RxEvent.EventGeoFence("exit"))
        }
    }
}