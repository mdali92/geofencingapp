package com.azhar.geofencingapp

import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import io.reactivex.disposables.Disposable
import java.util.HashMap
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    lateinit var geofencingClient: GeofencingClient

    var isGPSInsideGeoFence: Boolean = false
    var isWifiConnectedToGeoFence: Boolean = false

    private lateinit var disposable: Disposable

    companion object {
        const val REQUEST_LOCATION_PERMISSION_CODE:Int = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(!isLocationPermissionGranted()){
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_LOCATION_PERMISSION_CODE
            )
        } else {
            initializeGeoFencingClient()
        }

        button_start.setOnClickListener{
            startMonitoring()
        }

    }

    override fun onResume() {
        super.onResume()

        isGPSInsideGeoFence = false

        checkGeoFenceStatus()

        if(SharedPrefHelper.getLatitude(this) != 0.0 &&
            SharedPrefHelper.getLongitude(this) != 0.0 && isLocationPermissionGranted()){
            //Re-Register the GPS Geofencing here
            createGPSBasedGeoFence()
        }

        if(SharedPrefHelper.getWifiName(this).isNotEmpty())
            setUIForWifiStatus()

        createWifiStateBroadcastReceiver()

        disposable = RxBus.listen(RxEvent.EventGeoFence::class.java).subscribe {
            val event = it.geoFenceEvent
            if(event == "enter"){
                isGPSInsideGeoFence = true
            } else {
                isGPSInsideGeoFence = false
            }

            checkGeoFenceStatus()
        }

    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(wifiChangeReceiver)
    }

    override fun onDestroy() {
        super.onDestroy()

        if (!disposable.isDisposed) disposable.dispose()
    }

    private fun createWifiStateBroadcastReceiver(){
        val intentFilter = IntentFilter()
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION)
        registerReceiver(wifiChangeReceiver, intentFilter)
    }

    private val wifiChangeReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            Log.d("Azz","onReceive Wifi state")
            val action = intent.action
            if (action == WifiManager.NETWORK_STATE_CHANGED_ACTION) {
                val info = intent.getParcelableExtra<NetworkInfo>(WifiManager.EXTRA_NETWORK_INFO)
                val connected = info.isConnected()
                isWifiConnectedToGeoFence = if(connected){
                    val wifiManager = context.getApplicationContext().getSystemService(Context.WIFI_SERVICE) as WifiManager
                    val wifiInfo = wifiManager.connectionInfo
                    val ssid: String = wifiInfo.ssid
                    ssid == SharedPrefHelper.getWifiName(this@MainActivity)
                } else {
                    false
                }
            }
            checkGeoFenceStatus()
        }
    }

    private fun checkGeoFenceStatus() {
        if(isWifiConnectedToGeoFence || isGPSInsideGeoFence){
            tv_status.text = "STATUS : INSIDE GEOFENCE!!"
            tv_status.setTextColor(Color.parseColor("#008000"))
        } else {
            tv_status.text = "STATUS : OUTSIDE GEOFENCE!!"
            tv_status.setTextColor(Color.parseColor("#900020"))
        }
    }


    private fun isLocationPermissionGranted(): Boolean{
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private fun initializeGeoFencingClient(){
        geofencingClient = LocationServices.getGeofencingClient(this)
    }

    private fun startMonitoring() {
        if(et_latitude.text.toString().isNotEmpty() &&
            et_longitude.text.toString().isNotEmpty() &&
            et_radius.text.toString().isNotEmpty() &&
            isLocationPermissionGranted()){

            SharedPrefHelper.setLatitude(this,et_latitude.text.toString())
            SharedPrefHelper.setLongitude(this,et_longitude.text.toString())
            SharedPrefHelper.setRadius(this,et_radius.text.toString().toInt())

            Handler().postDelayed({
                createGPSBasedGeoFence()
            }, 500)
        }

        if(et_wifiname.text.toString().isNotEmpty()){
            SharedPrefHelper.setWifiName(this,et_wifiname.text.toString())
            setUIForWifiStatus()
        }
    }

    @SuppressLint("MissingPermission")
    private fun createGPSBasedGeoFence(){

        setUIForGPSGeofencingStatus()

        var geofenceList:ArrayList<Geofence> = ArrayList()
        geofenceList.add(
            Geofence.Builder()
                .setRequestId("setel")
                .setCircularRegion(
                    SharedPrefHelper.getLatitude(this),
                    SharedPrefHelper.getLongitude(this),
                    SharedPrefHelper.getRadius(this).toFloat()
                )
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER or Geofence.GEOFENCE_TRANSITION_EXIT)
                .build())

        val geoFencingRequest:GeofencingRequest = GeofencingRequest.Builder().apply {
            setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
            addGeofences(geofenceList)
        }.build()

        val geofencePendingIntent: PendingIntent by lazy {
            val intent = Intent(this, GeofenceBroadcastReceiver::class.java)
            PendingIntent.getBroadcast(this, 500, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        geofencingClient?.addGeofences(geoFencingRequest, geofencePendingIntent)?.run {
            addOnSuccessListener {
                Log.d("Azz","Geofence added -- " + " " + SharedPrefHelper.getLatitude(this@MainActivity) + " -- " + SharedPrefHelper.getLongitude(this@MainActivity) + " -- " + SharedPrefHelper.getRadius(this@MainActivity).toFloat())
            }
            addOnFailureListener {
                Log.d("Azz","Geofence added FAILED")
            }
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_LOCATION_PERMISSION_CODE) {
            val perms = HashMap<String, Int>()
            // Initialize the map with both permissions
            perms[Manifest.permission.ACCESS_FINE_LOCATION] = PackageManager.PERMISSION_GRANTED
            if (grantResults.isNotEmpty()) {
                for (i in permissions.indices)
                    perms[permissions[i]] = grantResults[i]
                if (perms[Manifest.permission.ACCESS_FINE_LOCATION] == PackageManager.PERMISSION_GRANTED) {
                    initializeGeoFencingClient()
                } else{
                    isGPSInsideGeoFence = false
                }
            } else {
                //Disable Lat and Lon EditTexts
                isGPSInsideGeoFence = false
            }
        }
    }

    private fun setUIForGPSGeofencingStatus(){
        tv_gps.setText("GPS : ("+SharedPrefHelper.getLatitude(this)+","+SharedPrefHelper.getLongitude(this)+")")
        tv_radius.setText("Radius(m) : "+SharedPrefHelper.getRadius(this))
    }

    private fun setUIForWifiStatus(){
        tv_wifiname.setText("Wifi : " + SharedPrefHelper.getWifiName(this))
    }

 }


