package com.azhar.geofencingapp

class RxEvent {
    data class EventGeoFence(val geoFenceEvent: String)
}