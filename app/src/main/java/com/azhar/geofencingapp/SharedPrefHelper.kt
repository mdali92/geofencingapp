package com.azhar.geofencingapp

import android.content.Context

object SharedPrefHelper {

    var PREF_NAME: String = "SharedPref"
    var PREF_MODE: Int = 0

    fun getLatitude(context: Context): Double{
        return context.getSharedPreferences(PREF_NAME, PREF_MODE).getString("latitude","0.0").toDouble()
    }

    fun setLatitude(context: Context, latitude: String){
        val editor = context.getSharedPreferences(PREF_NAME, PREF_MODE)!!.edit()
        editor.putString("latitude",latitude)
        editor.apply()
    }

    fun getLongitude(context: Context): Double{
        return context.getSharedPreferences(PREF_NAME, PREF_MODE).getString("longitude","0.0").toDouble()
    }

    fun setLongitude(context: Context, longitude: String){
        val editor = context.getSharedPreferences(PREF_NAME, PREF_MODE)!!.edit()
        editor.putString("longitude",longitude)
        editor.apply()
    }

    fun getRadius(context: Context): Int{
        return context.getSharedPreferences(PREF_NAME, PREF_MODE).getInt("radius",0)
    }

    fun setRadius(context: Context, radius: Int){
        val editor = context.getSharedPreferences(PREF_NAME, PREF_MODE)!!.edit()
        editor.putInt("radius",radius)
        editor.apply()
    }

    fun getWifiName(context: Context): String{
        return context.getSharedPreferences(PREF_NAME, PREF_MODE).getString("wifiname","")
    }

    fun setWifiName(context: Context, wifiname: String){
        val editor = context.getSharedPreferences(PREF_NAME, PREF_MODE)!!.edit()
        editor.putString("wifiname",wifiname)
        editor.apply()
    }

}